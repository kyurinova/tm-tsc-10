package ru.tsc.kyurinova.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}

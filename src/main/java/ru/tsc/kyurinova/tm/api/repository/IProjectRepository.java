package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
